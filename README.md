
**Create working hours component**

The goal of this assignment is to show your coding skills and what you value in software engineering. We value new ideas so next to the original requirement feel free to improve/add/extend. We evaluate the assignment depending on your role and your level of seniority.

**Challenge**

Create a component using your favorite backend technology (Angular - React - Vue) that represents the working hours of a store.

 1. The component should have 2 states (view - edit).
 
 2. You should have 2 routes, one for view and one for edit.
 
 3. Component input data will be fetched from external API endpoint, we created one for you (http://159.8.78.155:3001/api) and it accepts all HTTP requests.
 
 4. User should be able to change working hours or define a day as closed.
 
 5. If a day is closed start and finish times should be empty.
 
 6. Use can have the option to select all days at once.
 
 7. User should have the availability to define multiple start and finish times during a day, like if they're working on multiple shifts with a break in between the 2 shifts.
 
 8. You can change the JSON structure of *workingHours*as per you see fits the most.
 
 9. Different cases and user scenarios should be taken into consideration, we evaluate functionality, design for scalability, maintainability, and coverage.

**Requirements** 

 - Create and manage your application in a GIT-Repo from the start.
 
 - When committing, please provide a proper description.
 
 - Provide detailed description and documentation for your code.
 
 - State any external libraries if any.
